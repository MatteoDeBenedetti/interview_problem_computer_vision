/** @file main.cpp
 * Test for Space Applications candidates
 */

#include<dirent.h>
#include <iostream>
#include <fstream>
#include <opencv2/opencv.hpp>
#include "opencv2/xfeatures2d.hpp"


// Uncomment for debugging
//#define DEBUG


char getFolderSeparator()
{
  char sep = '/';
  #ifdef _WIN32
  sep = '\\';
  #endif
  return sep;
}

std::string getParentFolder(const std::string& s)
{
   size_t i = s.rfind(getFolderSeparator(), s.length());
   if (i != std::string::npos) {
      return(s.substr(0, i));
   }
   return("");
}

std::string createPath(const std::string& base_path, const std::string& folder)
{
  return std::string(base_path+getFolderSeparator()+folder);
}

void writeDetectionsToFile(const std::string &file_path, const std::string &detections)
{
  std::fstream file;
  file.open(createPath(file_path,"results.txt"), std::ios::out);
  file << detections<<std::endl;
  file.close();
}

std::vector<std::string> getImagesFromPath(const std::string &images_path)
{
  std::vector<std::string> files;
  DIR *dir;
  struct dirent *ent;
  if ((dir = opendir (images_path.c_str())) != NULL)
  {
    while ((ent = readdir (dir)) != NULL)
    {
      if (std::string(ent->d_name).size() > 3)
      {
        files.push_back(createPath(images_path, ent->d_name));
      }
    }
    closedir (dir);
  }
  else
  {
    perror ("Directory does not exist");
  }
  return files;
}


std::pair<cv::Point, cv::Point> detect(const cv::Mat& frame)
{
  /* Update this function with your detect logic */
  std::pair<cv::Point, cv::Point> bounding_box = std::make_pair(cv::Point(0, 0), cv::Point(100, 100)); // Initial value, to be updated

  // Add your code here

  // for debugging uncomment "#define DEBUG" on line 13

  // open image of the chosen can (can 1)
  cv::Mat train_frame_origina;
  train_frame_origina = cv::imread("../data/models_and_background/can1.jpg");

  // crop object of interest (can)
  cv::Rect myROI(2450, 900, 850, 1650); // can and a little background
  //cv::Rect myROI(2550, 900, 650, 1350); // can only
  //cv::Rect myROI(2600, 1150, 300, 400); // sauce only
  //cv::Rect myROI(2600, 900, 550, 1250); // can only and slightly cropped
  //cv::Rect myROI(2650, 1800, 450, 450); // label only
  cv::Mat train_frame = train_frame_origina(myROI);

#ifdef DEBUG
  // prepare variables for debug showing of intermediate steps
  cv::Mat frame_resized;
  cv::Size view_size(700,500);
#endif

  // convert train image to greyscale
  cv::cvtColor(train_frame, train_frame, CV_BGR2GRAY);
#ifdef DEBUG
  cv::resize(train_frame, frame_resized, view_size);
  cv::imshow("Greyscale", frame_resized);
  cv::waitKey();
  cv::destroyWindow("Greyscale");
#endif

  // apply sift detector and compute descriptor to train image
  cv::Ptr<cv::xfeatures2d::SIFT> sift_detector = cv::xfeatures2d::SIFT::create();
  std::vector<cv::KeyPoint> keypoints_train;
  cv::Mat descriptor_train;

  sift_detector->detectAndCompute(train_frame, cv::noArray(), keypoints_train, descriptor_train);


  // start processing the test image
  cv::Mat test_frame = frame;

  // convert test image to greyscale
  cv::cvtColor(test_frame, test_frame, CV_BGR2GRAY);
#ifdef DEBUG
  cv::resize(test_frame, frame_resized, view_size);
  cv::imshow("Greyscale", frame_resized);
  cv::waitKey();
  cv::destroyWindow("Greyscale");
#endif

  // apply sift detector and compute descriptor to test image
  std::vector<cv::KeyPoint> keypoints_test;
  cv::Mat descriptor_test;

  sift_detector->detectAndCompute(test_frame, cv::noArray(), keypoints_test, descriptor_test);

  // match features between train and test
  cv::Ptr<cv::DescriptorMatcher> matcher = cv::DescriptorMatcher::create(cv::DescriptorMatcher::FLANNBASED);
  std::vector<std::vector<cv::DMatch>> matches;
  matcher->knnMatch(descriptor_train, descriptor_test, matches, 2);

  // filter matches using the Lowe's ratio test
  const float ratio_thresh = 0.5f; //default 0.7f;
  std::vector<cv::DMatch> filtered_matches;
  for (size_t i = 0; i < matches.size(); i++)
  {
      if (matches[i][0].distance < ratio_thresh * matches[i][1].distance)
      {
          filtered_matches.push_back(matches[i][0]);
      }
  }

  // compute bounding box based on mnatches location
  std::vector<cv::Point> matched_points;

  for (size_t i = 0; i < filtered_matches.size(); i++)
  {
    matched_points.push_back(keypoints_test[filtered_matches[i].trainIdx].pt);
  }

  // find matches locations
  std::vector<int> matched_points_x;
  std::vector<int> matched_points_y;
  for (size_t i = 0; i < matched_points.size(); i++)
  {
    matched_points_x.push_back(matched_points[i].x);
    matched_points_y.push_back(matched_points[i].y);
  }

  // compute bounding box coordinates
  int min_x = *std::min_element(matched_points_x.begin(), matched_points_x.end());
  int min_y = *std::min_element(matched_points_y.begin(), matched_points_y.end());
  int max_x = *std::max_element(matched_points_x.begin(), matched_points_x.end());
  int max_y = *std::max_element(matched_points_y.begin(), matched_points_y.end());
#ifdef DEBUG
  std::cout << "min x: " << min_x << std::endl;
  std::cout << "min y: " << min_y << std::endl;
  std::cout << "max x: " << max_x << std::endl;
  std::cout << "max y: " << max_y << std::endl;
#endif

  bounding_box = std::make_pair(cv::Point(min_x, min_y), cv::Point(max_x, max_y));

  return bounding_box;
}


int main(int argc, char** argv)
{
  /* Do not modify this function */
  std::string detections = "";
  std::string file_path = __FILE__;
  std::string images_path = file_path.substr(0, file_path.rfind("\\"));
  images_path = createPath(getParentFolder(getParentFolder(images_path)), "data/test_images");
  auto files = getImagesFromPath(images_path);

  for (auto file : files)
  {
    cv::Mat frame = cv::imread(file, CV_LOAD_IMAGE_COLOR);
    if (frame.empty() == false)
    {
      auto bounding_box = detect(frame);
      detections +=  file +" "+std::to_string(bounding_box.first.x) \
                          +" "+std::to_string(bounding_box.first.y) \
                          +" "+std::to_string(bounding_box.second.x) \
                          +" "+std::to_string(bounding_box.second.y) +"\n";

      cv::rectangle(frame, bounding_box.first, bounding_box.second, cv::Scalar(255, 0, 0), 3);
      cv::imshow("Detection", frame);
      if (cv::waitKey(0) == 'q')
      {
        break;
      }
    }
  }

  writeDetectionsToFile(getParentFolder(file_path), detections);
  return 0;
}
