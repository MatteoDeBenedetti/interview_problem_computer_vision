#!/usr/bin/env python3
import os
from pathlib import Path
import cv2

"""
Test for Space Applications candidates
"""


def writeDetectionsToFile(file_path, detections):
    file = open(os.path.join(file_path, "results.txt"), "w+")
    file.write(detections)
    file.close()


def getImagesFromPath(images_path):
    files = []
    for r, d, f in os.walk(images_path):
        for file in f:
            files.append(os.path.join(r, file))

    files.sort()
    return files


def detect(frame):
    """ Update this function with your detect logic """
    bounding_box = [(0, 0), (100, 100)]  # Initial value, to be updated

    # Add your code here

    return bounding_box


def main():
    """ Do not modify this function """
    detections = ""
    script_path = Path(os.path.dirname(os.path.realpath(__file__)))
    images_path = os.path.join(script_path.parent, "data/test_images")
    files = getImagesFromPath(images_path)

    for file in files:
        frame = cv2.imread(file, cv2.IMREAD_COLOR)
        bounding_box = detect(frame)
        detections += file + " " + str(bounding_box[0][0]) \
                                 + " " + str(bounding_box[0][1]) \
                                 + " " + str(bounding_box[1][0]) \
                                 + " " + str(bounding_box[1][1]) + "\n"

        cv2.rectangle(frame, bounding_box[0], bounding_box[1], (255, 0, 0), 3)
        cv2.imshow('Detection', frame)
        if cv2.waitKey(0) & 0xFF == ord('q'):
            break

    writeDetectionsToFile(script_path, detections)


if __name__ == "__main__":
    main()
